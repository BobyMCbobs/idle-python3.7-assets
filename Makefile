SHELL:=/bin/bash
PREFIX ?= /usr/share

all: help

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/applications
	@mkdir -p $(DESTDIR)$(PREFIX)/pixmaps
	@cp -r idle-python3.7.desktop $(DESTDIR)$(PREFIX)/applications
	@cp -r python3.7.png $(DESTDIR)$(PREFIX)/pixmaps
	@chmod 644 $(DESTDIR)$(PREFIX)/applications/idle-python3.7.desktop
	@if [ $(DISTRO) = "openSUSE" ]; \
	then \
		sed -i -e "s/idle/idle3/g" $(DESTDIR)$(PREFIX)/applications/idle-python3.7.desktop; \
	fi

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/pixmaps/python3.7.png
	@rm -rf $(DESTDIR)$(PREFIX)/applications/idle-python3.7.desktop

help:
	@echo "[install|uninstall]"
