# idle-python3.7 for Arch Linux and openSUSE

## Installation  
Arch Linux: [AUR](https://aur.archlinux.org/idle-python3.7-assets).  
openSUSE: [openSUSE software](https://software.opensuse.org/package/idle-python3.7-assets).  

## Notes  
All rights reserved to the Python Software Foundation.  
This package is just for installing desktop and icon assets for use on Arch Linux.  
This package is also useless with using the package on the AUR (as stated in installation).  
Resources were scraped from the Ubuntu package.  
