#
# spec file for package idle-python3.7-assets
#
# Copyright (c) 2018 SUSE LINUX GmbH, Nuernberg, Germany.
# Copyright (c) 2018 Caleb Woodbine
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://bugs.opensuse.org/
#


Name:           idle-python3.7-assets
Version:        1.0.1
Release:        0%{?dist}
Summary:        Python's Intergrated Development and Learning Environment
BuildArch:	noarch
License:        GPL-3.0
Group:		Development/Languages/Python
URL:            https://gitlab.com/BobyMCbobs/%{name}
Source0:        https://gitlab.com/BobyMCbobs/%{name}/-/archive/%{version}/%{name}-%{version}.zip
%if %{defined suse_version}
Requires:       python3-idle
%endif
BuildRequires:	unzip
BuildRequires:  update-desktop-files
Requires(post): update-desktop-files
Requires(postun): update-desktop-files

%description
An intergrated development environment for Python. Written in TkInter.


%prep
%autosetup


%build


%install
%if %{defined suse_version}
%{__make} DESTDIR=$RPM_BUILD_ROOT DISTRO=openSUSE install
%else
%{__make} DESTDIR=$RPM_BUILD_ROOT install
%endif

%files
%license LICENSE
%doc README.md
%dir /usr/share/applications
%dir /usr/share/pixmaps
/usr/share/applications/idle-python3.7.desktop
/usr/share/pixmaps/python3.7.png


%changelog
* Fri May 25 2018 caleb
- Init to RPM
